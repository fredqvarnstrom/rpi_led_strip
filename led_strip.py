#!/usr/bin/env python
"""

USER: pi
PWD: 2b1/2mad
"""

import RPi.GPIO as GPIO
import sys
import time


class LEDStrip:

    pixels = []
    spi_port = '/dev/spidev0.0'

    def __init__(self, spi_port='/dev/spidev0.0', pixel_count=32):
        GPIO.setmode(GPIO.BCM)
        self.pixels = [0] * pixel_count
        self.spi_port = spi_port
        self.write_strip()

    def write_strip(self):
        """
        Write pixel data through SPI bus
        :return:
        """
        spidev = file(self.spi_port, "w")
        for i in range(len(self.pixels)):
            spidev.write(chr((self.pixels[i] >> 16) & 0xFF))
            spidev.write(chr((self.pixels[i] >> 8) & 0xFF))
            spidev.write(chr(self.pixels[i] & 0xFF))
        spidev.close()
        time.sleep(0.002)

    @staticmethod
    def get_color(r, g, b):
        return ((r & 0xFF) << 16) | ((g & 0xFF) << 8) | (b & 0xFF)

    def set_pixel_color(self, n, c):
        """
        Set color of a pixel
        :param n: pixel number, should be less than total pixel count
        :param c:
        :return:
        """
        if n >= len(self.pixels):
            return
        self.pixels[n] = c

    def color_wipe(self, c, delay):
        """
        Wipe whole pixels.
        :param c: Color
        :param delay: Delay for each pixel
        :return:
        """
        for i in range(len(self.pixels)):
            self.set_pixel_color(i, c)
            self.write_strip()
            time.sleep(delay)

    def wheel(self, pos):
        if pos < 85:
            return self.get_color(pos * 3, 255 - pos * 3, 0)
        elif pos < 170:
            pos -= 85
            return self.get_color(255 - pos * 3, 0, pos * 3)
        else:
            pos -= 170
            return self.get_color(0, pos * 3, 255 - pos * 3)

    def rainbow_cycle(self, wait):
        """
        Display rainbow.
        :param wait: Duration in seconds.
        :return:
        """
        for j in range(256):  # one cycle of all 256 colors in the wheel
            for i in range(len(self.pixels)):
                # tricky math! we use each pixel as a fraction of the full 96-color wheel
                # (that's the i / strip.numPixels() part)
                # Then add in j which makes the colors go around per pixel
                # the % 256 is to make the wheel cycle around
                self.set_pixel_color(i, self.wheel(((i * 256 / len(self.pixels)) + j) % 256))
        self.write_strip()
        time.sleep(wait)
 
    def cls(self):
        """
        Clear all pixels
        :return:
        """
        for i in range(len(self.pixels)):
            self.set_pixel_color(i, self.get_color(0, 0, 0))
        self.write_strip()

    def test_strip(self):
        try:
            self.color_wipe(self.get_color(255, 0, 0), 0.05)
            self.color_wipe(self.get_color(0, 255, 0), 0.05)
            self.color_wipe(self.get_color(0, 0, 255), 0.05)
            self.rainbow_cycle(3)
            self.cls()

        except KeyboardInterrupt:
            self.cls()
            sys.exit(0)

if __name__ == '__main__':
    led = LEDStrip(pixel_count=96)
    led.test_strip()
