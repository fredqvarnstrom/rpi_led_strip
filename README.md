## Gaiter Project

### Wiring Components.
    
| **Raspberry Pi** | **LED Strip**|
| :----:           |   :----:     |
|  GND             |    GND       | 
|  GPIO10(MOSI)    |    SI(DATA)  |
|  GPIO11(SCLK)    |    CK(CLOCK) |

- Connect the GND of RPi to the GND of +5V/6V Power.
- Connect 5V pin of LED Strip to the 5V pin of +5V/6V Power.

![Wiring Diagram](wiring.jpg "Wiring Diagram")

    
### Configuring RPi.
    
- Add `dtparam=spi=on` to `/boot/config.txt` and reboot.

- Run `sudo raspi-config` and enable SPI in `8 Advanced Options`.

### Install dependencies
    
    sudo pip install spidev

### Testing

    from led_strip import LEDStrip
    strip = LEDStrip(pixel_count=96)    # Delcare count of pixels, 32 pixel/m  
    strip.test_strip()                  # Test all pixels.
    

