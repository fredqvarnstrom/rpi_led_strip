
def height_to_step(height):
    """
    Get step length from height
    :param height: Height in cm
    :return:
    """
    if height < 140:
        return 49
    elif height < 146:
        return 50
    elif height < 148:
        return 51
    elif height < 150:
        return 52
    elif height < 154:
        return 53
    elif height < 156:
        return 54
    elif height < 160:
        return 55
    elif height < 162:
        return 56
    elif height < 166:
        return 57
    elif height < 168:
        return 58
    elif height < 170:
        return 59
    elif height < 174:
        return 60
    elif height < 176:
        return 61
    elif height < 180:
        return 62
    elif height < 182:
        return 63
    elif height < 186:
        return 64
    else:
        return 65

